package com.tremend.testingwizard.service;

import com.tremend.testingwizard.domain.*; // for static metamodels
import com.tremend.testingwizard.domain.LoyaltyCode;
import com.tremend.testingwizard.repository.LoyaltyCodeRepository;
import com.tremend.testingwizard.service.criteria.LoyaltyCodeCriteria;
import com.tremend.testingwizard.service.dto.LoyaltyCodeDTO;
import com.tremend.testingwizard.service.mapper.LoyaltyCodeMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link LoyaltyCode} entities in the database.
 * The main input is a {@link LoyaltyCodeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoyaltyCodeDTO} or a {@link Page} of {@link LoyaltyCodeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoyaltyCodeQueryService extends QueryService<LoyaltyCode> {

    private final Logger log = LoggerFactory.getLogger(LoyaltyCodeQueryService.class);

    private final LoyaltyCodeRepository loyaltyCodeRepository;

    private final LoyaltyCodeMapper loyaltyCodeMapper;

    public LoyaltyCodeQueryService(LoyaltyCodeRepository loyaltyCodeRepository, LoyaltyCodeMapper loyaltyCodeMapper) {
        this.loyaltyCodeRepository = loyaltyCodeRepository;
        this.loyaltyCodeMapper = loyaltyCodeMapper;
    }

    /**
     * Return a {@link List} of {@link LoyaltyCodeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoyaltyCodeDTO> findByCriteria(LoyaltyCodeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LoyaltyCode> specification = createSpecification(criteria);
        return loyaltyCodeMapper.toDto(loyaltyCodeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoyaltyCodeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoyaltyCodeDTO> findByCriteria(LoyaltyCodeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LoyaltyCode> specification = createSpecification(criteria);
        return loyaltyCodeRepository.findAll(specification, page).map(loyaltyCodeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoyaltyCodeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LoyaltyCode> specification = createSpecification(criteria);
        return loyaltyCodeRepository.count(specification);
    }

    /**
     * Function to convert {@link LoyaltyCodeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LoyaltyCode> createSpecification(LoyaltyCodeCriteria criteria) {
        Specification<LoyaltyCode> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LoyaltyCode_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), LoyaltyCode_.code));
            }
            if (criteria.getReward() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReward(), LoyaltyCode_.reward));
            }
            if (criteria.getShopName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getShopName(), LoyaltyCode_.shopName));
            }
        }
        return specification;
    }
}

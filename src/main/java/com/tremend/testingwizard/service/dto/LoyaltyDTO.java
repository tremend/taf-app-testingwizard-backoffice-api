package com.tremend.testingwizard.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link com.tremend.testingwizard.domain.Loyalty} entity.
 */
public class LoyaltyDTO implements Serializable {

    private Long id;

    private String code;

    private Integer reward;

    private Instant date;

    private String shopName;

    private UserDTO user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getReward() {
        return reward;
    }

    public void setReward(Integer reward) {
        this.reward = reward;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoyaltyDTO)) {
            return false;
        }

        LoyaltyDTO loyaltyDTO = (LoyaltyDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, loyaltyDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoyaltyDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", reward=" + getReward() +
            ", date='" + getDate() + "'" +
            ", shopName='" + getShopName() + "'" +
            ", user=" + getUser() +
            "}";
    }
}

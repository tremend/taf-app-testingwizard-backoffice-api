package com.tremend.testingwizard.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.tremend.testingwizard.domain.LoyaltyCode} entity.
 */
public class LoyaltyCodeDTO implements Serializable {

    private Long id;

    private String code;

    private Integer reward;

    private String shopName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getReward() {
        return reward;
    }

    public void setReward(Integer reward) {
        this.reward = reward;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoyaltyCodeDTO)) {
            return false;
        }

        LoyaltyCodeDTO loyaltyCodeDTO = (LoyaltyCodeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, loyaltyCodeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoyaltyCodeDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", reward=" + getReward() +
            ", shopName='" + getShopName() + "'" +
            "}";
    }
}

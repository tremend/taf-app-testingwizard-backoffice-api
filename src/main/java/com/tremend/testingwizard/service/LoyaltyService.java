package com.tremend.testingwizard.service;

import com.tremend.testingwizard.domain.Loyalty;
import com.tremend.testingwizard.repository.LoyaltyRepository;
import com.tremend.testingwizard.service.dto.LoyaltyDTO;
import com.tremend.testingwizard.service.mapper.LoyaltyMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Loyalty}.
 */
@Service
@Transactional
public class LoyaltyService {

    private final Logger log = LoggerFactory.getLogger(LoyaltyService.class);

    private final LoyaltyRepository loyaltyRepository;

    private final LoyaltyMapper loyaltyMapper;

    public LoyaltyService(LoyaltyRepository loyaltyRepository, LoyaltyMapper loyaltyMapper) {
        this.loyaltyRepository = loyaltyRepository;
        this.loyaltyMapper = loyaltyMapper;
    }

    /**
     * Save a loyalty.
     *
     * @param loyaltyDTO the entity to save.
     * @return the persisted entity.
     */
    public LoyaltyDTO save(LoyaltyDTO loyaltyDTO) {
        log.debug("Request to save Loyalty : {}", loyaltyDTO);
        Loyalty loyalty = loyaltyMapper.toEntity(loyaltyDTO);
        loyalty = loyaltyRepository.save(loyalty);
        return loyaltyMapper.toDto(loyalty);
    }

    /**
     * Partially update a loyalty.
     *
     * @param loyaltyDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<LoyaltyDTO> partialUpdate(LoyaltyDTO loyaltyDTO) {
        log.debug("Request to partially update Loyalty : {}", loyaltyDTO);

        return loyaltyRepository
            .findById(loyaltyDTO.getId())
            .map(
                existingLoyalty -> {
                    loyaltyMapper.partialUpdate(existingLoyalty, loyaltyDTO);

                    return existingLoyalty;
                }
            )
            .map(loyaltyRepository::save)
            .map(loyaltyMapper::toDto);
    }

    /**
     * Get all the loyalties.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LoyaltyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Loyalties");
        return loyaltyRepository.findAll(pageable).map(loyaltyMapper::toDto);
    }

    /**
     * Get one loyalty by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LoyaltyDTO> findOne(Long id) {
        log.debug("Request to get Loyalty : {}", id);
        return loyaltyRepository.findById(id).map(loyaltyMapper::toDto);
    }

    /**
     * Delete the loyalty by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Loyalty : {}", id);
        loyaltyRepository.deleteById(id);
    }
}

package com.tremend.testingwizard.service.mapper;

import com.tremend.testingwizard.domain.*;
import com.tremend.testingwizard.service.dto.LoyaltyDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Loyalty} and its DTO {@link LoyaltyDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface LoyaltyMapper extends EntityMapper<LoyaltyDTO, Loyalty> {
    @Mapping(target = "user", source = "user", qualifiedByName = "id")
    LoyaltyDTO toDto(Loyalty s);
}

package com.tremend.testingwizard.service.mapper;

import com.tremend.testingwizard.domain.*;
import com.tremend.testingwizard.service.dto.LoyaltyCodeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoyaltyCode} and its DTO {@link LoyaltyCodeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LoyaltyCodeMapper extends EntityMapper<LoyaltyCodeDTO, LoyaltyCode> {}

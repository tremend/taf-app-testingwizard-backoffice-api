package com.tremend.testingwizard.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.tremend.testingwizard.domain.Loyalty} entity. This class is used
 * in {@link com.tremend.testingwizard.web.rest.LoyaltyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loyalties?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LoyaltyCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private IntegerFilter reward;

    private InstantFilter date;

    private StringFilter shopName;

    private LongFilter userId;

    public LoyaltyCriteria() {}

    public LoyaltyCriteria(LoyaltyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.reward = other.reward == null ? null : other.reward.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.shopName = other.shopName == null ? null : other.shopName.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public LoyaltyCriteria copy() {
        return new LoyaltyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public StringFilter code() {
        if (code == null) {
            code = new StringFilter();
        }
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public IntegerFilter getReward() {
        return reward;
    }

    public IntegerFilter reward() {
        if (reward == null) {
            reward = new IntegerFilter();
        }
        return reward;
    }

    public void setReward(IntegerFilter reward) {
        this.reward = reward;
    }

    public InstantFilter getDate() {
        return date;
    }

    public InstantFilter date() {
        if (date == null) {
            date = new InstantFilter();
        }
        return date;
    }

    public void setDate(InstantFilter date) {
        this.date = date;
    }

    public StringFilter getShopName() {
        return shopName;
    }

    public StringFilter shopName() {
        if (shopName == null) {
            shopName = new StringFilter();
        }
        return shopName;
    }

    public void setShopName(StringFilter shopName) {
        this.shopName = shopName;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LoyaltyCriteria that = (LoyaltyCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(reward, that.reward) &&
            Objects.equals(date, that.date) &&
            Objects.equals(shopName, that.shopName) &&
            Objects.equals(userId, that.userId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, reward, date, shopName, userId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoyaltyCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (code != null ? "code=" + code + ", " : "") +
            (reward != null ? "reward=" + reward + ", " : "") +
            (date != null ? "date=" + date + ", " : "") +
            (shopName != null ? "shopName=" + shopName + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }
}

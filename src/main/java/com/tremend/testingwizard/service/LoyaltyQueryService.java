package com.tremend.testingwizard.service;

import com.tremend.testingwizard.domain.*; // for static metamodels
import com.tremend.testingwizard.domain.Loyalty;
import com.tremend.testingwizard.repository.LoyaltyRepository;
import com.tremend.testingwizard.service.criteria.LoyaltyCriteria;
import com.tremend.testingwizard.service.dto.LoyaltyDTO;
import com.tremend.testingwizard.service.mapper.LoyaltyMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Loyalty} entities in the database.
 * The main input is a {@link LoyaltyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoyaltyDTO} or a {@link Page} of {@link LoyaltyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoyaltyQueryService extends QueryService<Loyalty> {

    private final Logger log = LoggerFactory.getLogger(LoyaltyQueryService.class);

    private final LoyaltyRepository loyaltyRepository;

    private final LoyaltyMapper loyaltyMapper;

    public LoyaltyQueryService(LoyaltyRepository loyaltyRepository, LoyaltyMapper loyaltyMapper) {
        this.loyaltyRepository = loyaltyRepository;
        this.loyaltyMapper = loyaltyMapper;
    }

    /**
     * Return a {@link List} of {@link LoyaltyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoyaltyDTO> findByCriteria(LoyaltyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Loyalty> specification = createSpecification(criteria);
        return loyaltyMapper.toDto(loyaltyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoyaltyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoyaltyDTO> findByCriteria(LoyaltyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Loyalty> specification = createSpecification(criteria);
        return loyaltyRepository.findAll(specification, page).map(loyaltyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoyaltyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Loyalty> specification = createSpecification(criteria);
        return loyaltyRepository.count(specification);
    }

    /**
     * Function to convert {@link LoyaltyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Loyalty> createSpecification(LoyaltyCriteria criteria) {
        Specification<Loyalty> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Loyalty_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Loyalty_.code));
            }
            if (criteria.getReward() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReward(), Loyalty_.reward));
            }
            if (criteria.getDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDate(), Loyalty_.date));
            }
            if (criteria.getShopName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getShopName(), Loyalty_.shopName));
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(Loyalty_.user, JoinType.LEFT).get(User_.id))
                    );
            }
        }
        return specification;
    }
}

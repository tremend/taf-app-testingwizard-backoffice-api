package com.tremend.testingwizard.service;

import com.tremend.testingwizard.domain.LoyaltyCode;
import com.tremend.testingwizard.repository.LoyaltyCodeRepository;
import com.tremend.testingwizard.service.dto.LoyaltyCodeDTO;
import com.tremend.testingwizard.service.mapper.LoyaltyCodeMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link LoyaltyCode}.
 */
@Service
@Transactional
public class LoyaltyCodeService {

    private final Logger log = LoggerFactory.getLogger(LoyaltyCodeService.class);

    private final LoyaltyCodeRepository loyaltyCodeRepository;

    private final LoyaltyCodeMapper loyaltyCodeMapper;

    public LoyaltyCodeService(LoyaltyCodeRepository loyaltyCodeRepository, LoyaltyCodeMapper loyaltyCodeMapper) {
        this.loyaltyCodeRepository = loyaltyCodeRepository;
        this.loyaltyCodeMapper = loyaltyCodeMapper;
    }

    /**
     * Save a loyaltyCode.
     *
     * @param loyaltyCodeDTO the entity to save.
     * @return the persisted entity.
     */
    public LoyaltyCodeDTO save(LoyaltyCodeDTO loyaltyCodeDTO) {
        log.debug("Request to save LoyaltyCode : {}", loyaltyCodeDTO);
        LoyaltyCode loyaltyCode = loyaltyCodeMapper.toEntity(loyaltyCodeDTO);
        loyaltyCode = loyaltyCodeRepository.save(loyaltyCode);
        return loyaltyCodeMapper.toDto(loyaltyCode);
    }

    /**
     * Partially update a loyaltyCode.
     *
     * @param loyaltyCodeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<LoyaltyCodeDTO> partialUpdate(LoyaltyCodeDTO loyaltyCodeDTO) {
        log.debug("Request to partially update LoyaltyCode : {}", loyaltyCodeDTO);

        return loyaltyCodeRepository
            .findById(loyaltyCodeDTO.getId())
            .map(
                existingLoyaltyCode -> {
                    loyaltyCodeMapper.partialUpdate(existingLoyaltyCode, loyaltyCodeDTO);

                    return existingLoyaltyCode;
                }
            )
            .map(loyaltyCodeRepository::save)
            .map(loyaltyCodeMapper::toDto);
    }

    /**
     * Get all the loyaltyCodes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LoyaltyCodeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoyaltyCodes");
        return loyaltyCodeRepository.findAll(pageable).map(loyaltyCodeMapper::toDto);
    }

    /**
     * Get one loyaltyCode by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LoyaltyCodeDTO> findOne(Long id) {
        log.debug("Request to get LoyaltyCode : {}", id);
        return loyaltyCodeRepository.findById(id).map(loyaltyCodeMapper::toDto);
    }

    /**
     * Delete the loyaltyCode by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LoyaltyCode : {}", id);
        loyaltyCodeRepository.deleteById(id);
    }
}

package com.tremend.testingwizard.service;

import com.tremend.testingwizard.domain.*; // for static metamodels
import com.tremend.testingwizard.domain.Sms;
import com.tremend.testingwizard.repository.SmsRepository;
import com.tremend.testingwizard.service.criteria.SmsCriteria;
import com.tremend.testingwizard.service.dto.SmsDTO;
import com.tremend.testingwizard.service.mapper.SmsMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Sms} entities in the database.
 * The main input is a {@link SmsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SmsDTO} or a {@link Page} of {@link SmsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SmsQueryService extends QueryService<Sms> {

    private final Logger log = LoggerFactory.getLogger(SmsQueryService.class);

    private final SmsRepository smsRepository;

    private final SmsMapper smsMapper;

    public SmsQueryService(SmsRepository smsRepository, SmsMapper smsMapper) {
        this.smsRepository = smsRepository;
        this.smsMapper = smsMapper;
    }

    /**
     * Return a {@link List} of {@link SmsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SmsDTO> findByCriteria(SmsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sms> specification = createSpecification(criteria);
        return smsMapper.toDto(smsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SmsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SmsDTO> findByCriteria(SmsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sms> specification = createSpecification(criteria);
        return smsRepository.findAll(specification, page).map(smsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SmsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Sms> specification = createSpecification(criteria);
        return smsRepository.count(specification);
    }

    /**
     * Function to convert {@link SmsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Sms> createSpecification(SmsCriteria criteria) {
        Specification<Sms> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Sms_.id));
            }
            if (criteria.getPhoneNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNumber(), Sms_.phoneNumber));
            }
            if (criteria.getBody() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBody(), Sms_.body));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), Sms_.status));
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getUserId(), root -> root.join(Sms_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}

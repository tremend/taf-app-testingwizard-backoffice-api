package com.tremend.testingwizard.service.sms;

import com.tremend.testingwizard.web.rest.errors.SmsSenderException;

public interface SmsSender {
    void sendMessage(String to, String body) throws SmsSenderException;
}

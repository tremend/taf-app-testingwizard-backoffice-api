/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tremend.testingwizard.web.rest.vm;

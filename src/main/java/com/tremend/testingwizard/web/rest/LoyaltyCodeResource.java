package com.tremend.testingwizard.web.rest;

import com.tremend.testingwizard.repository.LoyaltyCodeRepository;
import com.tremend.testingwizard.service.LoyaltyCodeQueryService;
import com.tremend.testingwizard.service.LoyaltyCodeService;
import com.tremend.testingwizard.service.criteria.LoyaltyCodeCriteria;
import com.tremend.testingwizard.service.dto.LoyaltyCodeDTO;
import com.tremend.testingwizard.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.tremend.testingwizard.domain.LoyaltyCode}.
 */
@RestController
@RequestMapping("/api")
public class LoyaltyCodeResource {

    private final Logger log = LoggerFactory.getLogger(LoyaltyCodeResource.class);

    private static final String ENTITY_NAME = "loyaltyCode";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoyaltyCodeService loyaltyCodeService;

    private final LoyaltyCodeRepository loyaltyCodeRepository;

    private final LoyaltyCodeQueryService loyaltyCodeQueryService;

    public LoyaltyCodeResource(
        LoyaltyCodeService loyaltyCodeService,
        LoyaltyCodeRepository loyaltyCodeRepository,
        LoyaltyCodeQueryService loyaltyCodeQueryService
    ) {
        this.loyaltyCodeService = loyaltyCodeService;
        this.loyaltyCodeRepository = loyaltyCodeRepository;
        this.loyaltyCodeQueryService = loyaltyCodeQueryService;
    }

    /**
     * {@code POST  /loyalty-codes} : Create a new loyaltyCode.
     *
     * @param loyaltyCodeDTO the loyaltyCodeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loyaltyCodeDTO, or with status {@code 400 (Bad Request)} if the loyaltyCode has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loyalty-codes")
    public ResponseEntity<LoyaltyCodeDTO> createLoyaltyCode(@RequestBody LoyaltyCodeDTO loyaltyCodeDTO) throws URISyntaxException {
        log.debug("REST request to save LoyaltyCode : {}", loyaltyCodeDTO);
        if (loyaltyCodeDTO.getId() != null) {
            throw new BadRequestAlertException("A new loyaltyCode cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoyaltyCodeDTO result = loyaltyCodeService.save(loyaltyCodeDTO);
        return ResponseEntity
            .created(new URI("/api/loyalty-codes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loyalty-codes/:id} : Updates an existing loyaltyCode.
     *
     * @param id the id of the loyaltyCodeDTO to save.
     * @param loyaltyCodeDTO the loyaltyCodeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loyaltyCodeDTO,
     * or with status {@code 400 (Bad Request)} if the loyaltyCodeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loyaltyCodeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loyalty-codes/{id}")
    public ResponseEntity<LoyaltyCodeDTO> updateLoyaltyCode(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LoyaltyCodeDTO loyaltyCodeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update LoyaltyCode : {}, {}", id, loyaltyCodeDTO);
        if (loyaltyCodeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loyaltyCodeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loyaltyCodeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LoyaltyCodeDTO result = loyaltyCodeService.save(loyaltyCodeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loyaltyCodeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /loyalty-codes/:id} : Partial updates given fields of an existing loyaltyCode, field will ignore if it is null
     *
     * @param id the id of the loyaltyCodeDTO to save.
     * @param loyaltyCodeDTO the loyaltyCodeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loyaltyCodeDTO,
     * or with status {@code 400 (Bad Request)} if the loyaltyCodeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the loyaltyCodeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the loyaltyCodeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/loyalty-codes/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<LoyaltyCodeDTO> partialUpdateLoyaltyCode(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LoyaltyCodeDTO loyaltyCodeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update LoyaltyCode partially : {}, {}", id, loyaltyCodeDTO);
        if (loyaltyCodeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loyaltyCodeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loyaltyCodeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LoyaltyCodeDTO> result = loyaltyCodeService.partialUpdate(loyaltyCodeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loyaltyCodeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /loyalty-codes} : get all the loyaltyCodes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loyaltyCodes in body.
     */
    @GetMapping("/loyalty-codes")
    public ResponseEntity<List<LoyaltyCodeDTO>> getAllLoyaltyCodes(LoyaltyCodeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LoyaltyCodes by criteria: {}", criteria);
        Page<LoyaltyCodeDTO> page = loyaltyCodeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loyalty-codes/count} : count all the loyaltyCodes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loyalty-codes/count")
    public ResponseEntity<Long> countLoyaltyCodes(LoyaltyCodeCriteria criteria) {
        log.debug("REST request to count LoyaltyCodes by criteria: {}", criteria);
        return ResponseEntity.ok().body(loyaltyCodeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loyalty-codes/:id} : get the "id" loyaltyCode.
     *
     * @param id the id of the loyaltyCodeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loyaltyCodeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loyalty-codes/{id}")
    public ResponseEntity<LoyaltyCodeDTO> getLoyaltyCode(@PathVariable Long id) {
        log.debug("REST request to get LoyaltyCode : {}", id);
        Optional<LoyaltyCodeDTO> loyaltyCodeDTO = loyaltyCodeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loyaltyCodeDTO);
    }

    /**
     * {@code DELETE  /loyalty-codes/:id} : delete the "id" loyaltyCode.
     *
     * @param id the id of the loyaltyCodeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loyalty-codes/{id}")
    public ResponseEntity<Void> deleteLoyaltyCode(@PathVariable Long id) {
        log.debug("REST request to delete LoyaltyCode : {}", id);
        loyaltyCodeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

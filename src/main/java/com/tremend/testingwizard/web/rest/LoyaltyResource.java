package com.tremend.testingwizard.web.rest;

import com.tremend.testingwizard.repository.LoyaltyRepository;
import com.tremend.testingwizard.service.LoyaltyQueryService;
import com.tremend.testingwizard.service.LoyaltyService;
import com.tremend.testingwizard.service.criteria.LoyaltyCriteria;
import com.tremend.testingwizard.service.dto.LoyaltyDTO;
import com.tremend.testingwizard.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.tremend.testingwizard.domain.Loyalty}.
 */
@RestController
@RequestMapping("/api")
public class LoyaltyResource {

    private final Logger log = LoggerFactory.getLogger(LoyaltyResource.class);

    private static final String ENTITY_NAME = "loyalty";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoyaltyService loyaltyService;

    private final LoyaltyRepository loyaltyRepository;

    private final LoyaltyQueryService loyaltyQueryService;

    public LoyaltyResource(LoyaltyService loyaltyService, LoyaltyRepository loyaltyRepository, LoyaltyQueryService loyaltyQueryService) {
        this.loyaltyService = loyaltyService;
        this.loyaltyRepository = loyaltyRepository;
        this.loyaltyQueryService = loyaltyQueryService;
    }

    /**
     * {@code POST  /loyalties} : Create a new loyalty.
     *
     * @param loyaltyDTO the loyaltyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loyaltyDTO, or with status {@code 400 (Bad Request)} if the loyalty has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loyalties")
    public ResponseEntity<LoyaltyDTO> createLoyalty(@RequestBody LoyaltyDTO loyaltyDTO) throws URISyntaxException {
        log.debug("REST request to save Loyalty : {}", loyaltyDTO);
        if (loyaltyDTO.getId() != null) {
            throw new BadRequestAlertException("A new loyalty cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoyaltyDTO result = loyaltyService.save(loyaltyDTO);
        return ResponseEntity
            .created(new URI("/api/loyalties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loyalties/:id} : Updates an existing loyalty.
     *
     * @param id the id of the loyaltyDTO to save.
     * @param loyaltyDTO the loyaltyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loyaltyDTO,
     * or with status {@code 400 (Bad Request)} if the loyaltyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loyaltyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loyalties/{id}")
    public ResponseEntity<LoyaltyDTO> updateLoyalty(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LoyaltyDTO loyaltyDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Loyalty : {}, {}", id, loyaltyDTO);
        if (loyaltyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loyaltyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loyaltyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LoyaltyDTO result = loyaltyService.save(loyaltyDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loyaltyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /loyalties/:id} : Partial updates given fields of an existing loyalty, field will ignore if it is null
     *
     * @param id the id of the loyaltyDTO to save.
     * @param loyaltyDTO the loyaltyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loyaltyDTO,
     * or with status {@code 400 (Bad Request)} if the loyaltyDTO is not valid,
     * or with status {@code 404 (Not Found)} if the loyaltyDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the loyaltyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/loyalties/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<LoyaltyDTO> partialUpdateLoyalty(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LoyaltyDTO loyaltyDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Loyalty partially : {}, {}", id, loyaltyDTO);
        if (loyaltyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loyaltyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loyaltyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LoyaltyDTO> result = loyaltyService.partialUpdate(loyaltyDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loyaltyDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /loyalties} : get all the loyalties.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loyalties in body.
     */
    @GetMapping("/loyalties")
    public ResponseEntity<List<LoyaltyDTO>> getAllLoyalties(LoyaltyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Loyalties by criteria: {}", criteria);
        Page<LoyaltyDTO> page = loyaltyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loyalties/count} : count all the loyalties.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loyalties/count")
    public ResponseEntity<Long> countLoyalties(LoyaltyCriteria criteria) {
        log.debug("REST request to count Loyalties by criteria: {}", criteria);
        return ResponseEntity.ok().body(loyaltyQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loyalties/:id} : get the "id" loyalty.
     *
     * @param id the id of the loyaltyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loyaltyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loyalties/{id}")
    public ResponseEntity<LoyaltyDTO> getLoyalty(@PathVariable Long id) {
        log.debug("REST request to get Loyalty : {}", id);
        Optional<LoyaltyDTO> loyaltyDTO = loyaltyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loyaltyDTO);
    }

    /**
     * {@code DELETE  /loyalties/:id} : delete the "id" loyalty.
     *
     * @param id the id of the loyaltyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loyalties/{id}")
    public ResponseEntity<Void> deleteLoyalty(@PathVariable Long id) {
        log.debug("REST request to delete Loyalty : {}", id);
        loyaltyService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

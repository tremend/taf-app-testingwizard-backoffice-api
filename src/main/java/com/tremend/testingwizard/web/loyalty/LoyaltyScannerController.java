package com.tremend.testingwizard.web.loyalty;

import com.tremend.testingwizard.domain.LoyaltyCode;
import com.tremend.testingwizard.domain.User;
import com.tremend.testingwizard.repository.LoyaltyCodeRepository;
import com.tremend.testingwizard.service.LoyaltyService;
import com.tremend.testingwizard.service.UserService;
import com.tremend.testingwizard.service.dto.LoyaltyDTO;
import com.tremend.testingwizard.web.loyalty.dto.ScanDTO;
import com.tremend.testingwizard.web.rest.errors.BadRequestAlertException;
import java.util.Optional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class LoyaltyScannerController {

    private final UserService userService;
    private final LoyaltyService loyaltyService;
    private final LoyaltyCodeRepository loyaltyCodeRepository;

    public LoyaltyScannerController(LoyaltyService loyaltyService, LoyaltyCodeRepository loyaltyCodeRepository, UserService userService) {
        this.loyaltyService = loyaltyService;
        this.loyaltyCodeRepository = loyaltyCodeRepository;
        this.userService = userService;
    }

    @PostMapping("/loyalties/scan")
    public ResponseEntity<LoyaltyDTO> startWizard(@RequestBody ScanDTO scanDTO) {
        final Optional<User> userWithAuthorities = userService.getUserWithAuthorities();
        if (!userWithAuthorities.isPresent()) {
            throw new BadRequestAlertException("User not found", "User", "idnull");
        }
        final User user = userWithAuthorities.get();

        Optional<LoyaltyCode> loyaltyCodeOptional = loyaltyCodeRepository.findOneByCode(scanDTO.getCode());
        if (!loyaltyCodeOptional.isPresent()) {
            throw new BadRequestAlertException("Code not found", "Loyalty", "idnull");
        }
        final LoyaltyCode loyaltyCode = loyaltyCodeOptional.get();

        LoyaltyDTO loyalty = new LoyaltyDTO();
        loyalty.setCode(loyaltyCode.getCode());
        loyalty.setReward(loyaltyCode.getReward());
        //loyalty.setUserId(user.getId());
        loyalty.setShopName(loyaltyCode.getShopName());
        LoyaltyDTO result = loyaltyService.save(loyalty);
        return ResponseEntity.ok(result);
    }
}

package com.tremend.testingwizard.web.loyalty.dto;

import java.io.Serializable;

public class ScanDTO implements Serializable {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

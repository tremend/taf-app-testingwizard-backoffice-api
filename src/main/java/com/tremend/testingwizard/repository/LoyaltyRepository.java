package com.tremend.testingwizard.repository;

import com.tremend.testingwizard.domain.Loyalty;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Loyalty entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoyaltyRepository extends JpaRepository<Loyalty, Long>, JpaSpecificationExecutor<Loyalty> {
    @Query("select loyalty from Loyalty loyalty where loyalty.user.login = ?#{principal.username}")
    List<Loyalty> findByUserIsCurrentUser();
}

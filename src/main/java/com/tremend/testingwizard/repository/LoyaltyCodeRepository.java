package com.tremend.testingwizard.repository;

import com.tremend.testingwizard.domain.LoyaltyCode;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LoyaltyCode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoyaltyCodeRepository extends JpaRepository<LoyaltyCode, Long>, JpaSpecificationExecutor<LoyaltyCode> {
    Optional<LoyaltyCode> findOneByCode(String code);
}

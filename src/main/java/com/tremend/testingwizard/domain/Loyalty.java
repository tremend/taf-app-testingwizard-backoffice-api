package com.tremend.testingwizard.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A Loyalty.
 */
@Entity
@Table(name = "loyalty")
public class Loyalty implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "reward")
    private Integer reward;

    @Column(name = "date")
    private Instant date;

    @Column(name = "shop_name")
    private String shopName;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loyalty id(Long id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return this.code;
    }

    public Loyalty code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getReward() {
        return this.reward;
    }

    public Loyalty reward(Integer reward) {
        this.reward = reward;
        return this;
    }

    public void setReward(Integer reward) {
        this.reward = reward;
    }

    public Instant getDate() {
        return this.date;
    }

    public Loyalty date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getShopName() {
        return this.shopName;
    }

    public Loyalty shopName(String shopName) {
        this.shopName = shopName;
        return this;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public User getUser() {
        return this.user;
    }

    public Loyalty user(User user) {
        this.setUser(user);
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Loyalty)) {
            return false;
        }
        return id != null && id.equals(((Loyalty) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Loyalty{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", reward=" + getReward() +
            ", date='" + getDate() + "'" +
            ", shopName='" + getShopName() + "'" +
            "}";
    }
}

package com.tremend.testingwizard.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * A LoyaltyCode.
 */
@Entity
@Table(name = "loyalty_code")
public class LoyaltyCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "reward")
    private Integer reward;

    @Column(name = "shop_name")
    private String shopName;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoyaltyCode id(Long id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return this.code;
    }

    public LoyaltyCode code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getReward() {
        return this.reward;
    }

    public LoyaltyCode reward(Integer reward) {
        this.reward = reward;
        return this;
    }

    public void setReward(Integer reward) {
        this.reward = reward;
    }

    public String getShopName() {
        return this.shopName;
    }

    public LoyaltyCode shopName(String shopName) {
        this.shopName = shopName;
        return this;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoyaltyCode)) {
            return false;
        }
        return id != null && id.equals(((LoyaltyCode) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoyaltyCode{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", reward=" + getReward() +
            ", shopName='" + getShopName() + "'" +
            "}";
    }
}

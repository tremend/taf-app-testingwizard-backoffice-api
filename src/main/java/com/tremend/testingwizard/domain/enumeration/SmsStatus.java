package com.tremend.testingwizard.domain.enumeration;

/**
 * The SmsStatus enumeration.
 */
public enum SmsStatus {
    SUCCESS,
    FAILED,
    PENDING,
}

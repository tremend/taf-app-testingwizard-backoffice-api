package com.tremend.testingwizard.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sms")
@ConditionalOnProperty(prefix = "sms", name = "provider", havingValue = "twilio")
public class SmsConfigPropertiesTwilio {

    private String accountSid;
    private String authToken;
    private String from;
}

package com.tremend.testingwizard.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.tremend.testingwizard.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoyaltyCodeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoyaltyCodeDTO.class);
        LoyaltyCodeDTO loyaltyCodeDTO1 = new LoyaltyCodeDTO();
        loyaltyCodeDTO1.setId(1L);
        LoyaltyCodeDTO loyaltyCodeDTO2 = new LoyaltyCodeDTO();
        assertThat(loyaltyCodeDTO1).isNotEqualTo(loyaltyCodeDTO2);
        loyaltyCodeDTO2.setId(loyaltyCodeDTO1.getId());
        assertThat(loyaltyCodeDTO1).isEqualTo(loyaltyCodeDTO2);
        loyaltyCodeDTO2.setId(2L);
        assertThat(loyaltyCodeDTO1).isNotEqualTo(loyaltyCodeDTO2);
        loyaltyCodeDTO1.setId(null);
        assertThat(loyaltyCodeDTO1).isNotEqualTo(loyaltyCodeDTO2);
    }
}

package com.tremend.testingwizard.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.tremend.testingwizard.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoyaltyDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoyaltyDTO.class);
        LoyaltyDTO loyaltyDTO1 = new LoyaltyDTO();
        loyaltyDTO1.setId(1L);
        LoyaltyDTO loyaltyDTO2 = new LoyaltyDTO();
        assertThat(loyaltyDTO1).isNotEqualTo(loyaltyDTO2);
        loyaltyDTO2.setId(loyaltyDTO1.getId());
        assertThat(loyaltyDTO1).isEqualTo(loyaltyDTO2);
        loyaltyDTO2.setId(2L);
        assertThat(loyaltyDTO1).isNotEqualTo(loyaltyDTO2);
        loyaltyDTO1.setId(null);
        assertThat(loyaltyDTO1).isNotEqualTo(loyaltyDTO2);
    }
}

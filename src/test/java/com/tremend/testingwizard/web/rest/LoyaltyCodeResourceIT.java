package com.tremend.testingwizard.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tremend.testingwizard.IntegrationTest;
import com.tremend.testingwizard.domain.LoyaltyCode;
import com.tremend.testingwizard.repository.LoyaltyCodeRepository;
import com.tremend.testingwizard.service.criteria.LoyaltyCodeCriteria;
import com.tremend.testingwizard.service.dto.LoyaltyCodeDTO;
import com.tremend.testingwizard.service.mapper.LoyaltyCodeMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LoyaltyCodeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LoyaltyCodeResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_REWARD = 1;
    private static final Integer UPDATED_REWARD = 2;
    private static final Integer SMALLER_REWARD = 1 - 1;

    private static final String DEFAULT_SHOP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SHOP_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/loyalty-codes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LoyaltyCodeRepository loyaltyCodeRepository;

    @Autowired
    private LoyaltyCodeMapper loyaltyCodeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoyaltyCodeMockMvc;

    private LoyaltyCode loyaltyCode;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoyaltyCode createEntity(EntityManager em) {
        LoyaltyCode loyaltyCode = new LoyaltyCode().code(DEFAULT_CODE).reward(DEFAULT_REWARD).shopName(DEFAULT_SHOP_NAME);
        return loyaltyCode;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoyaltyCode createUpdatedEntity(EntityManager em) {
        LoyaltyCode loyaltyCode = new LoyaltyCode().code(UPDATED_CODE).reward(UPDATED_REWARD).shopName(UPDATED_SHOP_NAME);
        return loyaltyCode;
    }

    @BeforeEach
    public void initTest() {
        loyaltyCode = createEntity(em);
    }

    @Test
    @Transactional
    void createLoyaltyCode() throws Exception {
        int databaseSizeBeforeCreate = loyaltyCodeRepository.findAll().size();
        // Create the LoyaltyCode
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);
        restLoyaltyCodeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeCreate + 1);
        LoyaltyCode testLoyaltyCode = loyaltyCodeList.get(loyaltyCodeList.size() - 1);
        assertThat(testLoyaltyCode.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testLoyaltyCode.getReward()).isEqualTo(DEFAULT_REWARD);
        assertThat(testLoyaltyCode.getShopName()).isEqualTo(DEFAULT_SHOP_NAME);
    }

    @Test
    @Transactional
    void createLoyaltyCodeWithExistingId() throws Exception {
        // Create the LoyaltyCode with an existing ID
        loyaltyCode.setId(1L);
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);

        int databaseSizeBeforeCreate = loyaltyCodeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoyaltyCodeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodes() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList
        restLoyaltyCodeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loyaltyCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].reward").value(hasItem(DEFAULT_REWARD)))
            .andExpect(jsonPath("$.[*].shopName").value(hasItem(DEFAULT_SHOP_NAME)));
    }

    @Test
    @Transactional
    void getLoyaltyCode() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get the loyaltyCode
        restLoyaltyCodeMockMvc
            .perform(get(ENTITY_API_URL_ID, loyaltyCode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loyaltyCode.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.reward").value(DEFAULT_REWARD))
            .andExpect(jsonPath("$.shopName").value(DEFAULT_SHOP_NAME));
    }

    @Test
    @Transactional
    void getLoyaltyCodesByIdFiltering() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        Long id = loyaltyCode.getId();

        defaultLoyaltyCodeShouldBeFound("id.equals=" + id);
        defaultLoyaltyCodeShouldNotBeFound("id.notEquals=" + id);

        defaultLoyaltyCodeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoyaltyCodeShouldNotBeFound("id.greaterThan=" + id);

        defaultLoyaltyCodeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoyaltyCodeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where code equals to DEFAULT_CODE
        defaultLoyaltyCodeShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the loyaltyCodeList where code equals to UPDATED_CODE
        defaultLoyaltyCodeShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where code not equals to DEFAULT_CODE
        defaultLoyaltyCodeShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the loyaltyCodeList where code not equals to UPDATED_CODE
        defaultLoyaltyCodeShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where code in DEFAULT_CODE or UPDATED_CODE
        defaultLoyaltyCodeShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the loyaltyCodeList where code equals to UPDATED_CODE
        defaultLoyaltyCodeShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where code is not null
        defaultLoyaltyCodeShouldBeFound("code.specified=true");

        // Get all the loyaltyCodeList where code is null
        defaultLoyaltyCodeShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByCodeContainsSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where code contains DEFAULT_CODE
        defaultLoyaltyCodeShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the loyaltyCodeList where code contains UPDATED_CODE
        defaultLoyaltyCodeShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where code does not contain DEFAULT_CODE
        defaultLoyaltyCodeShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the loyaltyCodeList where code does not contain UPDATED_CODE
        defaultLoyaltyCodeShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward equals to DEFAULT_REWARD
        defaultLoyaltyCodeShouldBeFound("reward.equals=" + DEFAULT_REWARD);

        // Get all the loyaltyCodeList where reward equals to UPDATED_REWARD
        defaultLoyaltyCodeShouldNotBeFound("reward.equals=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward not equals to DEFAULT_REWARD
        defaultLoyaltyCodeShouldNotBeFound("reward.notEquals=" + DEFAULT_REWARD);

        // Get all the loyaltyCodeList where reward not equals to UPDATED_REWARD
        defaultLoyaltyCodeShouldBeFound("reward.notEquals=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsInShouldWork() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward in DEFAULT_REWARD or UPDATED_REWARD
        defaultLoyaltyCodeShouldBeFound("reward.in=" + DEFAULT_REWARD + "," + UPDATED_REWARD);

        // Get all the loyaltyCodeList where reward equals to UPDATED_REWARD
        defaultLoyaltyCodeShouldNotBeFound("reward.in=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsNullOrNotNull() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward is not null
        defaultLoyaltyCodeShouldBeFound("reward.specified=true");

        // Get all the loyaltyCodeList where reward is null
        defaultLoyaltyCodeShouldNotBeFound("reward.specified=false");
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward is greater than or equal to DEFAULT_REWARD
        defaultLoyaltyCodeShouldBeFound("reward.greaterThanOrEqual=" + DEFAULT_REWARD);

        // Get all the loyaltyCodeList where reward is greater than or equal to UPDATED_REWARD
        defaultLoyaltyCodeShouldNotBeFound("reward.greaterThanOrEqual=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward is less than or equal to DEFAULT_REWARD
        defaultLoyaltyCodeShouldBeFound("reward.lessThanOrEqual=" + DEFAULT_REWARD);

        // Get all the loyaltyCodeList where reward is less than or equal to SMALLER_REWARD
        defaultLoyaltyCodeShouldNotBeFound("reward.lessThanOrEqual=" + SMALLER_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsLessThanSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward is less than DEFAULT_REWARD
        defaultLoyaltyCodeShouldNotBeFound("reward.lessThan=" + DEFAULT_REWARD);

        // Get all the loyaltyCodeList where reward is less than UPDATED_REWARD
        defaultLoyaltyCodeShouldBeFound("reward.lessThan=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByRewardIsGreaterThanSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where reward is greater than DEFAULT_REWARD
        defaultLoyaltyCodeShouldNotBeFound("reward.greaterThan=" + DEFAULT_REWARD);

        // Get all the loyaltyCodeList where reward is greater than SMALLER_REWARD
        defaultLoyaltyCodeShouldBeFound("reward.greaterThan=" + SMALLER_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByShopNameIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where shopName equals to DEFAULT_SHOP_NAME
        defaultLoyaltyCodeShouldBeFound("shopName.equals=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyCodeList where shopName equals to UPDATED_SHOP_NAME
        defaultLoyaltyCodeShouldNotBeFound("shopName.equals=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByShopNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where shopName not equals to DEFAULT_SHOP_NAME
        defaultLoyaltyCodeShouldNotBeFound("shopName.notEquals=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyCodeList where shopName not equals to UPDATED_SHOP_NAME
        defaultLoyaltyCodeShouldBeFound("shopName.notEquals=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByShopNameIsInShouldWork() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where shopName in DEFAULT_SHOP_NAME or UPDATED_SHOP_NAME
        defaultLoyaltyCodeShouldBeFound("shopName.in=" + DEFAULT_SHOP_NAME + "," + UPDATED_SHOP_NAME);

        // Get all the loyaltyCodeList where shopName equals to UPDATED_SHOP_NAME
        defaultLoyaltyCodeShouldNotBeFound("shopName.in=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByShopNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where shopName is not null
        defaultLoyaltyCodeShouldBeFound("shopName.specified=true");

        // Get all the loyaltyCodeList where shopName is null
        defaultLoyaltyCodeShouldNotBeFound("shopName.specified=false");
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByShopNameContainsSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where shopName contains DEFAULT_SHOP_NAME
        defaultLoyaltyCodeShouldBeFound("shopName.contains=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyCodeList where shopName contains UPDATED_SHOP_NAME
        defaultLoyaltyCodeShouldNotBeFound("shopName.contains=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltyCodesByShopNameNotContainsSomething() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        // Get all the loyaltyCodeList where shopName does not contain DEFAULT_SHOP_NAME
        defaultLoyaltyCodeShouldNotBeFound("shopName.doesNotContain=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyCodeList where shopName does not contain UPDATED_SHOP_NAME
        defaultLoyaltyCodeShouldBeFound("shopName.doesNotContain=" + UPDATED_SHOP_NAME);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoyaltyCodeShouldBeFound(String filter) throws Exception {
        restLoyaltyCodeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loyaltyCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].reward").value(hasItem(DEFAULT_REWARD)))
            .andExpect(jsonPath("$.[*].shopName").value(hasItem(DEFAULT_SHOP_NAME)));

        // Check, that the count call also returns 1
        restLoyaltyCodeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoyaltyCodeShouldNotBeFound(String filter) throws Exception {
        restLoyaltyCodeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoyaltyCodeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLoyaltyCode() throws Exception {
        // Get the loyaltyCode
        restLoyaltyCodeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewLoyaltyCode() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();

        // Update the loyaltyCode
        LoyaltyCode updatedLoyaltyCode = loyaltyCodeRepository.findById(loyaltyCode.getId()).get();
        // Disconnect from session so that the updates on updatedLoyaltyCode are not directly saved in db
        em.detach(updatedLoyaltyCode);
        updatedLoyaltyCode.code(UPDATED_CODE).reward(UPDATED_REWARD).shopName(UPDATED_SHOP_NAME);
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(updatedLoyaltyCode);

        restLoyaltyCodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loyaltyCodeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isOk());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
        LoyaltyCode testLoyaltyCode = loyaltyCodeList.get(loyaltyCodeList.size() - 1);
        assertThat(testLoyaltyCode.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testLoyaltyCode.getReward()).isEqualTo(UPDATED_REWARD);
        assertThat(testLoyaltyCode.getShopName()).isEqualTo(UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void putNonExistingLoyaltyCode() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();
        loyaltyCode.setId(count.incrementAndGet());

        // Create the LoyaltyCode
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoyaltyCodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loyaltyCodeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLoyaltyCode() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();
        loyaltyCode.setId(count.incrementAndGet());

        // Create the LoyaltyCode
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyCodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLoyaltyCode() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();
        loyaltyCode.setId(count.incrementAndGet());

        // Create the LoyaltyCode
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyCodeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLoyaltyCodeWithPatch() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();

        // Update the loyaltyCode using partial update
        LoyaltyCode partialUpdatedLoyaltyCode = new LoyaltyCode();
        partialUpdatedLoyaltyCode.setId(loyaltyCode.getId());

        partialUpdatedLoyaltyCode.shopName(UPDATED_SHOP_NAME);

        restLoyaltyCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoyaltyCode.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoyaltyCode))
            )
            .andExpect(status().isOk());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
        LoyaltyCode testLoyaltyCode = loyaltyCodeList.get(loyaltyCodeList.size() - 1);
        assertThat(testLoyaltyCode.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testLoyaltyCode.getReward()).isEqualTo(DEFAULT_REWARD);
        assertThat(testLoyaltyCode.getShopName()).isEqualTo(UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void fullUpdateLoyaltyCodeWithPatch() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();

        // Update the loyaltyCode using partial update
        LoyaltyCode partialUpdatedLoyaltyCode = new LoyaltyCode();
        partialUpdatedLoyaltyCode.setId(loyaltyCode.getId());

        partialUpdatedLoyaltyCode.code(UPDATED_CODE).reward(UPDATED_REWARD).shopName(UPDATED_SHOP_NAME);

        restLoyaltyCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoyaltyCode.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoyaltyCode))
            )
            .andExpect(status().isOk());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
        LoyaltyCode testLoyaltyCode = loyaltyCodeList.get(loyaltyCodeList.size() - 1);
        assertThat(testLoyaltyCode.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testLoyaltyCode.getReward()).isEqualTo(UPDATED_REWARD);
        assertThat(testLoyaltyCode.getShopName()).isEqualTo(UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingLoyaltyCode() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();
        loyaltyCode.setId(count.incrementAndGet());

        // Create the LoyaltyCode
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoyaltyCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, loyaltyCodeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLoyaltyCode() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();
        loyaltyCode.setId(count.incrementAndGet());

        // Create the LoyaltyCode
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLoyaltyCode() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyCodeRepository.findAll().size();
        loyaltyCode.setId(count.incrementAndGet());

        // Create the LoyaltyCode
        LoyaltyCodeDTO loyaltyCodeDTO = loyaltyCodeMapper.toDto(loyaltyCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyCodeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(loyaltyCodeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoyaltyCode in the database
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLoyaltyCode() throws Exception {
        // Initialize the database
        loyaltyCodeRepository.saveAndFlush(loyaltyCode);

        int databaseSizeBeforeDelete = loyaltyCodeRepository.findAll().size();

        // Delete the loyaltyCode
        restLoyaltyCodeMockMvc
            .perform(delete(ENTITY_API_URL_ID, loyaltyCode.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoyaltyCode> loyaltyCodeList = loyaltyCodeRepository.findAll();
        assertThat(loyaltyCodeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

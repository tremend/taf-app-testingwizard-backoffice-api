package com.tremend.testingwizard.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tremend.testingwizard.IntegrationTest;
import com.tremend.testingwizard.domain.Loyalty;
import com.tremend.testingwizard.domain.User;
import com.tremend.testingwizard.repository.LoyaltyRepository;
import com.tremend.testingwizard.service.criteria.LoyaltyCriteria;
import com.tremend.testingwizard.service.dto.LoyaltyDTO;
import com.tremend.testingwizard.service.mapper.LoyaltyMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LoyaltyResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LoyaltyResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_REWARD = 1;
    private static final Integer UPDATED_REWARD = 2;
    private static final Integer SMALLER_REWARD = 1 - 1;

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_SHOP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SHOP_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/loyalties";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LoyaltyRepository loyaltyRepository;

    @Autowired
    private LoyaltyMapper loyaltyMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoyaltyMockMvc;

    private Loyalty loyalty;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Loyalty createEntity(EntityManager em) {
        Loyalty loyalty = new Loyalty().code(DEFAULT_CODE).reward(DEFAULT_REWARD).date(DEFAULT_DATE).shopName(DEFAULT_SHOP_NAME);
        return loyalty;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Loyalty createUpdatedEntity(EntityManager em) {
        Loyalty loyalty = new Loyalty().code(UPDATED_CODE).reward(UPDATED_REWARD).date(UPDATED_DATE).shopName(UPDATED_SHOP_NAME);
        return loyalty;
    }

    @BeforeEach
    public void initTest() {
        loyalty = createEntity(em);
    }

    @Test
    @Transactional
    void createLoyalty() throws Exception {
        int databaseSizeBeforeCreate = loyaltyRepository.findAll().size();
        // Create the Loyalty
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);
        restLoyaltyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loyaltyDTO)))
            .andExpect(status().isCreated());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeCreate + 1);
        Loyalty testLoyalty = loyaltyList.get(loyaltyList.size() - 1);
        assertThat(testLoyalty.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testLoyalty.getReward()).isEqualTo(DEFAULT_REWARD);
        assertThat(testLoyalty.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testLoyalty.getShopName()).isEqualTo(DEFAULT_SHOP_NAME);
    }

    @Test
    @Transactional
    void createLoyaltyWithExistingId() throws Exception {
        // Create the Loyalty with an existing ID
        loyalty.setId(1L);
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);

        int databaseSizeBeforeCreate = loyaltyRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoyaltyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loyaltyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllLoyalties() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList
        restLoyaltyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loyalty.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].reward").value(hasItem(DEFAULT_REWARD)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].shopName").value(hasItem(DEFAULT_SHOP_NAME)));
    }

    @Test
    @Transactional
    void getLoyalty() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get the loyalty
        restLoyaltyMockMvc
            .perform(get(ENTITY_API_URL_ID, loyalty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loyalty.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.reward").value(DEFAULT_REWARD))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.shopName").value(DEFAULT_SHOP_NAME));
    }

    @Test
    @Transactional
    void getLoyaltiesByIdFiltering() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        Long id = loyalty.getId();

        defaultLoyaltyShouldBeFound("id.equals=" + id);
        defaultLoyaltyShouldNotBeFound("id.notEquals=" + id);

        defaultLoyaltyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoyaltyShouldNotBeFound("id.greaterThan=" + id);

        defaultLoyaltyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoyaltyShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where code equals to DEFAULT_CODE
        defaultLoyaltyShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the loyaltyList where code equals to UPDATED_CODE
        defaultLoyaltyShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where code not equals to DEFAULT_CODE
        defaultLoyaltyShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the loyaltyList where code not equals to UPDATED_CODE
        defaultLoyaltyShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where code in DEFAULT_CODE or UPDATED_CODE
        defaultLoyaltyShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the loyaltyList where code equals to UPDATED_CODE
        defaultLoyaltyShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where code is not null
        defaultLoyaltyShouldBeFound("code.specified=true");

        // Get all the loyaltyList where code is null
        defaultLoyaltyShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllLoyaltiesByCodeContainsSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where code contains DEFAULT_CODE
        defaultLoyaltyShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the loyaltyList where code contains UPDATED_CODE
        defaultLoyaltyShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where code does not contain DEFAULT_CODE
        defaultLoyaltyShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the loyaltyList where code does not contain UPDATED_CODE
        defaultLoyaltyShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward equals to DEFAULT_REWARD
        defaultLoyaltyShouldBeFound("reward.equals=" + DEFAULT_REWARD);

        // Get all the loyaltyList where reward equals to UPDATED_REWARD
        defaultLoyaltyShouldNotBeFound("reward.equals=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward not equals to DEFAULT_REWARD
        defaultLoyaltyShouldNotBeFound("reward.notEquals=" + DEFAULT_REWARD);

        // Get all the loyaltyList where reward not equals to UPDATED_REWARD
        defaultLoyaltyShouldBeFound("reward.notEquals=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsInShouldWork() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward in DEFAULT_REWARD or UPDATED_REWARD
        defaultLoyaltyShouldBeFound("reward.in=" + DEFAULT_REWARD + "," + UPDATED_REWARD);

        // Get all the loyaltyList where reward equals to UPDATED_REWARD
        defaultLoyaltyShouldNotBeFound("reward.in=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsNullOrNotNull() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward is not null
        defaultLoyaltyShouldBeFound("reward.specified=true");

        // Get all the loyaltyList where reward is null
        defaultLoyaltyShouldNotBeFound("reward.specified=false");
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward is greater than or equal to DEFAULT_REWARD
        defaultLoyaltyShouldBeFound("reward.greaterThanOrEqual=" + DEFAULT_REWARD);

        // Get all the loyaltyList where reward is greater than or equal to UPDATED_REWARD
        defaultLoyaltyShouldNotBeFound("reward.greaterThanOrEqual=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward is less than or equal to DEFAULT_REWARD
        defaultLoyaltyShouldBeFound("reward.lessThanOrEqual=" + DEFAULT_REWARD);

        // Get all the loyaltyList where reward is less than or equal to SMALLER_REWARD
        defaultLoyaltyShouldNotBeFound("reward.lessThanOrEqual=" + SMALLER_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsLessThanSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward is less than DEFAULT_REWARD
        defaultLoyaltyShouldNotBeFound("reward.lessThan=" + DEFAULT_REWARD);

        // Get all the loyaltyList where reward is less than UPDATED_REWARD
        defaultLoyaltyShouldBeFound("reward.lessThan=" + UPDATED_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByRewardIsGreaterThanSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where reward is greater than DEFAULT_REWARD
        defaultLoyaltyShouldNotBeFound("reward.greaterThan=" + DEFAULT_REWARD);

        // Get all the loyaltyList where reward is greater than SMALLER_REWARD
        defaultLoyaltyShouldBeFound("reward.greaterThan=" + SMALLER_REWARD);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByDateIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where date equals to DEFAULT_DATE
        defaultLoyaltyShouldBeFound("date.equals=" + DEFAULT_DATE);

        // Get all the loyaltyList where date equals to UPDATED_DATE
        defaultLoyaltyShouldNotBeFound("date.equals=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where date not equals to DEFAULT_DATE
        defaultLoyaltyShouldNotBeFound("date.notEquals=" + DEFAULT_DATE);

        // Get all the loyaltyList where date not equals to UPDATED_DATE
        defaultLoyaltyShouldBeFound("date.notEquals=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByDateIsInShouldWork() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where date in DEFAULT_DATE or UPDATED_DATE
        defaultLoyaltyShouldBeFound("date.in=" + DEFAULT_DATE + "," + UPDATED_DATE);

        // Get all the loyaltyList where date equals to UPDATED_DATE
        defaultLoyaltyShouldNotBeFound("date.in=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where date is not null
        defaultLoyaltyShouldBeFound("date.specified=true");

        // Get all the loyaltyList where date is null
        defaultLoyaltyShouldNotBeFound("date.specified=false");
    }

    @Test
    @Transactional
    void getAllLoyaltiesByShopNameIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where shopName equals to DEFAULT_SHOP_NAME
        defaultLoyaltyShouldBeFound("shopName.equals=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyList where shopName equals to UPDATED_SHOP_NAME
        defaultLoyaltyShouldNotBeFound("shopName.equals=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByShopNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where shopName not equals to DEFAULT_SHOP_NAME
        defaultLoyaltyShouldNotBeFound("shopName.notEquals=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyList where shopName not equals to UPDATED_SHOP_NAME
        defaultLoyaltyShouldBeFound("shopName.notEquals=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByShopNameIsInShouldWork() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where shopName in DEFAULT_SHOP_NAME or UPDATED_SHOP_NAME
        defaultLoyaltyShouldBeFound("shopName.in=" + DEFAULT_SHOP_NAME + "," + UPDATED_SHOP_NAME);

        // Get all the loyaltyList where shopName equals to UPDATED_SHOP_NAME
        defaultLoyaltyShouldNotBeFound("shopName.in=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByShopNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where shopName is not null
        defaultLoyaltyShouldBeFound("shopName.specified=true");

        // Get all the loyaltyList where shopName is null
        defaultLoyaltyShouldNotBeFound("shopName.specified=false");
    }

    @Test
    @Transactional
    void getAllLoyaltiesByShopNameContainsSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where shopName contains DEFAULT_SHOP_NAME
        defaultLoyaltyShouldBeFound("shopName.contains=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyList where shopName contains UPDATED_SHOP_NAME
        defaultLoyaltyShouldNotBeFound("shopName.contains=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByShopNameNotContainsSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        // Get all the loyaltyList where shopName does not contain DEFAULT_SHOP_NAME
        defaultLoyaltyShouldNotBeFound("shopName.doesNotContain=" + DEFAULT_SHOP_NAME);

        // Get all the loyaltyList where shopName does not contain UPDATED_SHOP_NAME
        defaultLoyaltyShouldBeFound("shopName.doesNotContain=" + UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void getAllLoyaltiesByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        loyalty.setUser(user);
        loyaltyRepository.saveAndFlush(loyalty);
        Long userId = user.getId();

        // Get all the loyaltyList where user equals to userId
        defaultLoyaltyShouldBeFound("userId.equals=" + userId);

        // Get all the loyaltyList where user equals to (userId + 1)
        defaultLoyaltyShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoyaltyShouldBeFound(String filter) throws Exception {
        restLoyaltyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loyalty.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].reward").value(hasItem(DEFAULT_REWARD)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].shopName").value(hasItem(DEFAULT_SHOP_NAME)));

        // Check, that the count call also returns 1
        restLoyaltyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoyaltyShouldNotBeFound(String filter) throws Exception {
        restLoyaltyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoyaltyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLoyalty() throws Exception {
        // Get the loyalty
        restLoyaltyMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewLoyalty() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();

        // Update the loyalty
        Loyalty updatedLoyalty = loyaltyRepository.findById(loyalty.getId()).get();
        // Disconnect from session so that the updates on updatedLoyalty are not directly saved in db
        em.detach(updatedLoyalty);
        updatedLoyalty.code(UPDATED_CODE).reward(UPDATED_REWARD).date(UPDATED_DATE).shopName(UPDATED_SHOP_NAME);
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(updatedLoyalty);

        restLoyaltyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loyaltyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyDTO))
            )
            .andExpect(status().isOk());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
        Loyalty testLoyalty = loyaltyList.get(loyaltyList.size() - 1);
        assertThat(testLoyalty.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testLoyalty.getReward()).isEqualTo(UPDATED_REWARD);
        assertThat(testLoyalty.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testLoyalty.getShopName()).isEqualTo(UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void putNonExistingLoyalty() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();
        loyalty.setId(count.incrementAndGet());

        // Create the Loyalty
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoyaltyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loyaltyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLoyalty() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();
        loyalty.setId(count.incrementAndGet());

        // Create the Loyalty
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLoyalty() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();
        loyalty.setId(count.incrementAndGet());

        // Create the Loyalty
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loyaltyDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLoyaltyWithPatch() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();

        // Update the loyalty using partial update
        Loyalty partialUpdatedLoyalty = new Loyalty();
        partialUpdatedLoyalty.setId(loyalty.getId());

        partialUpdatedLoyalty.code(UPDATED_CODE).shopName(UPDATED_SHOP_NAME);

        restLoyaltyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoyalty.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoyalty))
            )
            .andExpect(status().isOk());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
        Loyalty testLoyalty = loyaltyList.get(loyaltyList.size() - 1);
        assertThat(testLoyalty.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testLoyalty.getReward()).isEqualTo(DEFAULT_REWARD);
        assertThat(testLoyalty.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testLoyalty.getShopName()).isEqualTo(UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void fullUpdateLoyaltyWithPatch() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();

        // Update the loyalty using partial update
        Loyalty partialUpdatedLoyalty = new Loyalty();
        partialUpdatedLoyalty.setId(loyalty.getId());

        partialUpdatedLoyalty.code(UPDATED_CODE).reward(UPDATED_REWARD).date(UPDATED_DATE).shopName(UPDATED_SHOP_NAME);

        restLoyaltyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoyalty.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoyalty))
            )
            .andExpect(status().isOk());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
        Loyalty testLoyalty = loyaltyList.get(loyaltyList.size() - 1);
        assertThat(testLoyalty.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testLoyalty.getReward()).isEqualTo(UPDATED_REWARD);
        assertThat(testLoyalty.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testLoyalty.getShopName()).isEqualTo(UPDATED_SHOP_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingLoyalty() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();
        loyalty.setId(count.incrementAndGet());

        // Create the Loyalty
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoyaltyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, loyaltyDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLoyalty() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();
        loyalty.setId(count.incrementAndGet());

        // Create the Loyalty
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loyaltyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLoyalty() throws Exception {
        int databaseSizeBeforeUpdate = loyaltyRepository.findAll().size();
        loyalty.setId(count.incrementAndGet());

        // Create the Loyalty
        LoyaltyDTO loyaltyDTO = loyaltyMapper.toDto(loyalty);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoyaltyMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(loyaltyDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Loyalty in the database
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLoyalty() throws Exception {
        // Initialize the database
        loyaltyRepository.saveAndFlush(loyalty);

        int databaseSizeBeforeDelete = loyaltyRepository.findAll().size();

        // Delete the loyalty
        restLoyaltyMockMvc
            .perform(delete(ENTITY_API_URL_ID, loyalty.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Loyalty> loyaltyList = loyaltyRepository.findAll();
        assertThat(loyaltyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

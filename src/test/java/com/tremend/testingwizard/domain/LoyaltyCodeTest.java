package com.tremend.testingwizard.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.tremend.testingwizard.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoyaltyCodeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoyaltyCode.class);
        LoyaltyCode loyaltyCode1 = new LoyaltyCode();
        loyaltyCode1.setId(1L);
        LoyaltyCode loyaltyCode2 = new LoyaltyCode();
        loyaltyCode2.setId(loyaltyCode1.getId());
        assertThat(loyaltyCode1).isEqualTo(loyaltyCode2);
        loyaltyCode2.setId(2L);
        assertThat(loyaltyCode1).isNotEqualTo(loyaltyCode2);
        loyaltyCode1.setId(null);
        assertThat(loyaltyCode1).isNotEqualTo(loyaltyCode2);
    }
}

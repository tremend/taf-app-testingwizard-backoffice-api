package com.tremend.testingwizard.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.tremend.testingwizard.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoyaltyTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Loyalty.class);
        Loyalty loyalty1 = new Loyalty();
        loyalty1.setId(1L);
        Loyalty loyalty2 = new Loyalty();
        loyalty2.setId(loyalty1.getId());
        assertThat(loyalty1).isEqualTo(loyalty2);
        loyalty2.setId(2L);
        assertThat(loyalty1).isNotEqualTo(loyalty2);
        loyalty1.setId(null);
        assertThat(loyalty1).isNotEqualTo(loyalty2);
    }
}

package com.tremend.testingwizard;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.tremend.testingwizard");

        noClasses()
            .that()
            .resideInAnyPackage("com.tremend.testingwizard.service..")
            .or()
            .resideInAnyPackage("com.tremend.testingwizard.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.tremend.testingwizard.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
